import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './helpers/loginActive.service';

const routes: Routes = [
  { path: '', component: LoginComponent},
  { path: 'home', loadChildren: () => import('./homepage/homepage.module').then((m) => m.HomepageModule), canActivate: [AuthGuard]}
];
@NgModule({
  imports: [RouterModule.forRoot(routes,{ 
    preloadingStrategy: PreloadAllModules 
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
