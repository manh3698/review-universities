import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FacService {

  constructor(private httpClient: HttpClient) { }
  public getFac(){
    return this.httpClient.get(`http://3.12.85.162/faculty`);
  }
  public delFac(id){
    return this.httpClient.delete(`http://3.12.85.162/faculty/${id}`)
  }
  public addFac(data: any){
    return this.httpClient.post(`http://3.12.85.162/faculty`, data)
  }
  public getUni(){
    return this.httpClient.get(`http://3.12.85.162/uni`)
  }
  public editFac(data: any, id){
    return this.httpClient.put(`http://3.12.85.162/faculty/${id}`, data)
  }
}
