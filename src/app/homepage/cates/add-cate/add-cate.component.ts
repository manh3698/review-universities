import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { CateService } from '../cate.service';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
// import { error } from 'console';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-add-cate',
  templateUrl: './add-cate.component.html',
  styleUrls: ['./add-cate.component.css']
})
export class AddCateComponent implements OnInit {

  addCate : FormGroup;
  constructor(private cateService: CateService,private router: Router, public fb: FormBuilder, private http : Http) { 
    this.addCate = this.fb.group({
      name: [''],
      logo: ['']
    })
  }

  imageObj: File;
  imageUrl: String;

  ngOnInit(): void {
    
  }

  uploadFile(event){
    const FILE = (event.target as HTMLInputElement).files[0];
    this.imageObj = FILE;
  }
  onSubmit(){
    const imageForm = new FormData;
    imageForm.append('image', this.imageObj);
    this.cateService.upload(imageForm).subscribe((res : any) => {
      this.addCate.value.logo = res.data._id;
      this.cateService.addCate(this.addCate.value).subscribe((data : any) => {
        alert('add success');
        this.router.navigateByUrl('add-cate');
        location.reload();
      });
    },(error: HttpErrorResponse) => {
      console.log(error.error);
    });
  }
  
}
