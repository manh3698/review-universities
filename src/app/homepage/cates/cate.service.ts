import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map'
@Injectable({
  providedIn: 'root'
})
export class CateService {
  httpOptions = {
    headers: new HttpHeaders({
     'Content-Type': 'multipart/form-data' 
    })
  };
  data: any;

  constructor(private httpClient: HttpClient) { }

  public getCate(){
    return this.httpClient.get('http://3.12.85.162/cate');
  }

  public search(keyword: string){
    return this.httpClient.get('http://3.12.85.162/cate/' + "?keyword="+keyword);
  }

  public addCate(data: any){
    return this.httpClient.post(`http://3.12.85.162/cate`, data)
  }

  public delCate(id){
    return this.httpClient.delete(`http://3.12.85.162/cate/${id}`)
  }

  public editCate(data: any, id){
    return this.httpClient.put(`http://3.12.85.162/cate/${id}`,data)
  }

  public upload(file) {
    return this.httpClient.post('http://3.12.85.162/image', file);
  }
}


// export class CateModel{
//   _id : string;
//   email : string;
//   name : string;
//   role : string;
// }
