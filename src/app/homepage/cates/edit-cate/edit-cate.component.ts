import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { CateService } from '../cate.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Http } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-edit-cate',
  templateUrl: './edit-cate.component.html',
  styleUrls: ['./edit-cate.component.css']
})
export class EditCateComponent implements OnInit {

  editCate : FormGroup;
  constructor(private cateService: CateService,private route: ActivatedRoute,private router: Router ,public fb: FormBuilder, private http : Http) { 
    this.editCate = this.fb.group({
      name: [''],
      logo: ['']
    })
  }
  imageObj: File;
  imageUrl: string;
  ngOnInit(): void {
  }
  uploadFile(event) {
    const FILE = (event.target as HTMLInputElement).files[0];
    this.imageObj = FILE;
  }

  onSubmit(){
    const imageForm = new FormData();
    imageForm.append('image', this.imageObj);
    const id = this.route.snapshot.paramMap.get('id');
    
    this.cateService.upload(imageForm).subscribe(
      (response: any) => {
        this.editCate.value.logo = response.data._id;
        this.cateService.editCate(this.editCate.value, id).subscribe((data: any) => {
          this.router.navigateByUrl('cate');
        });
      },
      (error: HttpErrorResponse) => {
        console.log(error.error);
      });
  }
}
