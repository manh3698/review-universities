import { Component, OnInit } from '@angular/core';
import { CateService } from '../cate.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-cate',
  templateUrl: './list-cate.component.html',
  styleUrls: ['./list-cate.component.css']
})
export class ListCateComponent implements OnInit {

  listCate;
  collection = [];
  public keyword: string;
  p: number = 1;
  constructor(private cateService: CateService, private router: Router) {}
  
  Search(){
    this.cateService.search(this.keyword).subscribe((data: any) => {
      this.listCate = data.data.data;
    });
  }

  ngOnInit(): void {
    this.cateService.getCate().subscribe((data: any) => {
      this.listCate = data.data.data;
    });
  }
  onDelete(id){
    this.cateService.delCate(id).subscribe((data: any)=> {
      this.router.navigateByUrl('cate');
    })

  }

}
