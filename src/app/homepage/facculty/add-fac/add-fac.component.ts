import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { FacService } from '../fac.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-fac',
  templateUrl: './add-fac.component.html',
  styleUrls: ['./add-fac.component.css']
})
export class AddFacComponent implements OnInit {

  addFac = new FormGroup({
    name: new FormControl(''),
    benchmarking: new FormControl(''),
    idUniver: new FormControl('')
  });
  constructor(private facService: FacService , private router : Router) { }

  listUni;
  ngOnInit(): void {
    this.facService.getUni().subscribe((data: any)=>{
      this.listUni = data.data.data;
    })
  }

  onSubmit(){
    this.facService.addFac(this.addFac.value).subscribe((data: any)=>{
      this.router.navigateByUrl('fac');
    })

  }
}
