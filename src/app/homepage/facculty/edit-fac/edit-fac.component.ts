import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { FacService } from '../fac.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-fac',
  templateUrl: './edit-fac.component.html',
  styleUrls: ['./edit-fac.component.css']
})
export class EditFacComponent implements OnInit {

  editFac = new FormGroup({
    name: new FormControl(''),
    benchmarking: new FormControl(''),
    idUniver: new FormControl('')
  });
  constructor(private facService: FacService, private route: ActivatedRoute) { }
  listUni;

  ngOnInit(): void {
    this.facService.getUni().subscribe((data: any)=>{
      this.listUni = data.data.data;
    })
  }

  onSubmit(){
    const id = this.route.snapshot.paramMap.get('id');
    this.facService.editFac(this.editFac.value, id).subscribe((data: any)=>{
      alert('edit success');
      location.reload();
    })
  }
}
