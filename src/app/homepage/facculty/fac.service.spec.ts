import { TestBed } from '@angular/core/testing';

import { FacService } from './fac.service';

describe('FacService', () => {
  let service: FacService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FacService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
