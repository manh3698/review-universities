import { Component, OnInit } from '@angular/core';
import { FacService, Fac } from '../fac.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-fac',
  templateUrl: './list-fac.component.html',
  styleUrls: ['./list-fac.component.css']
})
export class ListFacComponent implements OnInit {

  listFac;
  page: number = 1;
  constructor(private facService: FacService  ,  private router : Router) { }

  ngOnInit(): void {
    this.facService.getFac().subscribe((data: any)=>{
      this.listFac = data.data.data;
    })
  }
  onDelete(fac : Fac){
    this.facService.delFac(fac._id).subscribe((data: any)=>{
      this.router.navigateByUrl('fac');
      this.listFac = this.listFac.filter(u => u !== fac);
    })
  }

}
