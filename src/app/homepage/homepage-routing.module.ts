import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './homepage.component';
import { ListCateComponent } from './cates/list-cate/list-cate.component';
import { ListUserComponent } from './user/list-user/list-user.component';
import { ListFacComponent } from './facculty/list-fac/list-fac.component';
import { ListReviewComponent } from './review/list-review/list-review.component';
import { ListRateComponent } from './rate/list-rate/list-rate.component';
import { AddUniComponent } from './university/add-uni/add-uni.component';
import { EditUniComponent } from './university/edit-uni/edit-uni.component';
import { ListUniComponent } from './university/list-uni/list-uni.component';
import { AuthGuard } from '../helpers/loginActive.service';
import { AddFacComponent } from './facculty/add-fac/add-fac.component';
import { EditFacComponent } from './facculty/edit-fac/edit-fac.component';
import { AddReviewComponent } from './review/add-review/add-review.component';
import { EditCateComponent } from './cates/edit-cate/edit-cate.component';
import { AddCateComponent } from './cates/add-cate/add-cate.component';
import { AddUserComponent } from './user/add-user/add-user.component';
import { EditUserComponent } from './user/edit-user/edit-user.component';
import { LoginComponent } from '../login/login.component';
import { EditRateComponent } from './rate/edit-rate/edit-rate.component';
import { AddRateComponent } from './rate/add-rate/add-rate.component';
import { EditReviewComponent } from './review/edit-review/edit-review.component';

const routes: Routes = [
  { path: '', component: HomepageComponent,
    children: [
      { path: '', component: ListCateComponent, canActivate: [AuthGuard]},
      { path: 'cate', component: ListCateComponent, canActivate: [AuthGuard]},
      { path: 'user', component: ListUserComponent, canActivate: [AuthGuard]},
      { path: 'fac', component: ListFacComponent, canActivate: [AuthGuard]},
      { path: 'review', component: ListReviewComponent, canActivate: [AuthGuard]},
      { path: 'add-review', component: AddReviewComponent, canActivate: [AuthGuard]},

      { path: 'rate', component: ListRateComponent, canActivate: [AuthGuard]},
      { path: 'add-uni', component: AddUniComponent, canActivate: [AuthGuard]},
      { path: 'edit-uni/:id', component: EditUniComponent, canActivate: [AuthGuard]},
      { path: 'uni', component: ListUniComponent, canActivate: [AuthGuard]},
      { path: 'add-fac', component: AddFacComponent, canActivate: [AuthGuard]},
      { path: 'edit-fac/:id', component: EditFacComponent, canActivate: [AuthGuard]},
      { path: 'edit-cate/:id', component: EditCateComponent, canActivate: [AuthGuard]},
      { path: 'add-cate', component: AddCateComponent, canActivate: [AuthGuard]},
      {path : 'edit-rate/:id', component: EditRateComponent,canActivate:[AuthGuard]},
      {path : 'add-rate', component: AddRateComponent,canActivate:[AuthGuard]},
      { path : 'add-user', component: AddUserComponent,canActivate:[AuthGuard]},
      { path : 'edit-user/:id', component: EditUserComponent,canActivate:[AuthGuard]},
      { path : 'login', component: LoginComponent,canActivate:[AuthGuard]},
      {path : 'edit-review/:id', component: EditReviewComponent,canActivate:[AuthGuard]},
      {path : 'add-review', component: AddReviewComponent,canActivate:[AuthGuard]},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]

})
export class HomepageRoutingModule {

}





