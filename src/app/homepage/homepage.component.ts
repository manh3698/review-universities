import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ListCateComponent} from './cates/list-cate/list-cate.component';
import { from } from 'rxjs';
import { CateService } from './cates/cate.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  User;
  listCate;
  public keyword: string;
  constructor(private cateService: CateService, private router: Router, private route: ActivatedRoute) { }
  ngOnInit() {
    
  }
  Logout(){
    localStorage.clear();
    this.router.navigateByUrl("/login");
  }

  Search(){
    this.cateService.search(this.keyword).subscribe((data: any) => {
      this.listCate = data.data.data;
      this.router.navigateByUrl("/cate");
    });
  }
}
