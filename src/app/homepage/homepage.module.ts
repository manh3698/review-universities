import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HomepageRoutingModule } from './homepage-routing.module';
import { HomepageComponent } from './homepage.component';
import { ListUserComponent } from './user/list-user/list-user.component';
import { EditUserComponent } from './user/edit-user/edit-user.component';
import { AddUserComponent } from './user/add-user/add-user.component';
import { AddUniComponent } from './university/add-uni/add-uni.component';
import { EditUniComponent } from './university/edit-uni/edit-uni.component';
import { ListUniComponent } from './university/list-uni/list-uni.component';
import { AddFacComponent } from './facculty/add-fac/add-fac.component';
import { EditFacComponent } from './facculty/edit-fac/edit-fac.component';
import { ListFacComponent } from './facculty/list-fac/list-fac.component';
import { AddReviewComponent } from './review/add-review/add-review.component';
import { EditReviewComponent } from './review/edit-review/edit-review.component';
import { ListReviewComponent } from './review/list-review/list-review.component';
import { AddRateComponent } from './rate/add-rate/add-rate.component';
import { EditRateComponent } from './rate/edit-rate/edit-rate.component';
import { ListRateComponent } from './rate/list-rate/list-rate.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddCateComponent } from './cates/add-cate/add-cate.component';
import { EditCateComponent } from './cates/edit-cate/edit-cate.component';
import { ListCateComponent } from './cates/list-cate/list-cate.component';
import { NgxPaginationModule} from 'ngx-pagination';
import { TokenInterceptor } from '../helpers/TokenInterceptor';
import { JsonpModule , HttpModule} from '@angular/http';



@NgModule({
  declarations: [
    HomepageComponent,
    ListUserComponent,
    EditUserComponent,
    AddUserComponent,
    AddUniComponent,
    EditUniComponent,
    ListUniComponent,
    AddFacComponent,
    EditFacComponent,
    ListFacComponent,
    AddReviewComponent,
    EditReviewComponent,
    ListReviewComponent,
    AddRateComponent,
    EditRateComponent,
    ListRateComponent,
    AddCateComponent,
    EditCateComponent,
    ListCateComponent,
    AddRateComponent,
  ],
  imports: [
    CommonModule,
    HomepageRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    JsonpModule,
    HttpModule

  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ]
})
export class HomepageModule {}
