import { Component, OnInit } from '@angular/core';
import { RateService } from '../rate.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/map'


@Component({
  selector: 'app-add-rate',
  templateUrl: './add-rate.component.html',
  styleUrls: ['./add-rate.component.css']
})
export class AddRateComponent implements OnInit {

  addRateForm : FormGroup;
  listRate;
  listUser ;
  listUni ;
  constructor(private rateService : RateService , public formBuilder : FormBuilder ,private route : Router, private router : ActivatedRoute) {
            this.addRateForm = this.formBuilder.group({
              univerId : String,
              scoreQuality : Number,
              scoreMaterial : Number,
              scoreWork : Number,
              scoreTeaching : Number,
            });
        }

     ngOnInit(): void {
    this.rateService.getUni().subscribe((data: any)=>{
      this.listUni = data.data.data;
    });
  }

  onAddRate(){
    this.rateService.addRating(this.addRateForm.value).subscribe((data)=>{
      this.route.navigateByUrl('rate');
    },(error: HttpErrorResponse)=>{
        console.log(error);
    })
  }

}
