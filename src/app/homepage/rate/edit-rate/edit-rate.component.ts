import { Component, OnInit } from '@angular/core';

import { RateService } from '../rate.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/map'
import { NUMBER_TYPE } from '@angular/compiler/src/output/output_ast';



@Component({
  selector: 'app-edit-rate',
  templateUrl: './edit-rate.component.html',
  styleUrls: ['./edit-rate.component.css']
})
export class EditRateComponent implements OnInit {
  editRateForm : FormGroup;
  constructor(private rateService : RateService , public formBuilder : FormBuilder ,private route : Router, private router : ActivatedRoute) {
            this.editRateForm = this.formBuilder.group({
              scoreQuality : Number,
              scoreMaterial : Number,
              scoreTeaching : Number,
              scoreWork : Number,
              average : Number ,
            });
        }

  ngOnInit(): void {

  }

  onEditRate(){
    const id = this.router.snapshot.paramMap.get('id');
    this.editRateForm.value.scoreQuality = this.editRateForm.value.scoreQuality
    this.rateService.editRating(this.editRateForm.value , id).subscribe((data)=>{
      this.route.navigateByUrl('rate');
    },(error: HttpErrorResponse)=>{
        console.log(error);
    })

  }

}
