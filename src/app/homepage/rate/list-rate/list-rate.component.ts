import { Component, OnInit } from '@angular/core';
import { RateService, Rating } from '../rate.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-rate',
  templateUrl: './list-rate.component.html',
  styleUrls: ['./list-rate.component.css']
})
export class ListRateComponent implements OnInit {

  listRate;
  page: number = 1;

  constructor(private RateService : RateService , private router : Router) { }

  ngOnInit(): void {
    this.RateService.listRating().subscribe((data:any)=>{
      this.listRate = data.data;
      console.log( "Hello" + this.listRate[0].average);
    })
  }

  onDelete(rating : Rating){
    this.RateService.deleteRating(rating._id).subscribe(
      (data : any) =>{
        this.router.navigateByUrl('rate');
      })
      this.listRate = this.listRate.filter(u => u !== rating);
    }
}
