import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RateService {

  httpOptions = {
    headers: new HttpHeaders({
     'Content-Type': 'multipart/form-data' //
    })
  };

  constructor(private httpClient: HttpClient) { }

  public addRating(rating: Rating){
  return this.httpClient.post(`http://3.12.85.162/rating`, rating);
}

public listRating(){
  return this.httpClient.get('http://3.12.85.162/rating');
}
public deleteRating(id: string){
  return this.httpClient.delete('http://3.12.85.162/rating' + '/' + id);
}

public editRating(data : any, id){
  return this.httpClient.put(`http://3.12.85.162/rating/${id}`, data);
}

public getUni(){
  return this.httpClient.get('http://3.12.85.162/uni');
}



}



export class Rating{
  _id : string;
  scoreQuality : number;
  scoreMaterial : number;
  scoreWork : number;
  scoreTeaching : number;
  average : number;
  univerId : string;
  author : string;
}
