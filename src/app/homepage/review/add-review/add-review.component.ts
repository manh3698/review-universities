import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../api.service';


import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/map'
import { ReviewService } from '../review.service';
import { strict } from 'assert';


@Component({
  selector: 'app-add-review',
  templateUrl: './add-review.component.html',
  styleUrls: ['./add-review.component.css']
})
export class AddReviewComponent implements OnInit {
  listUni ;
  arrayImage : Array<File> = [];
  myFiles: string [] = [];
  //image : File ;
  imageUrl: string;
  addRateForm :  FormGroup;
  imagesData = []

  constructor(private reviewServces : ReviewService , public formBuilder : FormBuilder ,private router : Router, private route : ActivatedRoute) {
    this.addRateForm = this.formBuilder.group({
      idUniver : String,
      content : [''],
      images : [],
    });
}

  ngOnInit(): void {
    this.reviewServces.getUni().subscribe((data: any)=>{
      this.listUni = data.data.data;
    });
  }

  uploadFile(event) {
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < event.target.files.length; i++) {
      this.myFiles.push(event.target.files[i]);
      console.log(this.myFiles);
  }

  }

  onAddRate() {
    const formData = new FormData();
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.myFiles.length; i++) {
      formData.append('images', this.myFiles[i]);
    }


    this.reviewServces.uploadmutiple(formData).subscribe(
      (response: any) => {
        for(let i = 0 ; i < response.data.length ; i++){
          console.log(response.data[i])
          this.imagesData.push(response.data[i]._id)
        }
        this.addRateForm.value.images = this.imagesData

        this.reviewServces.addreview(this.addRateForm.value).subscribe((data) => {
          console.log(data)
          alert("add success");
          this.router.navigateByUrl('review');
        });
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      });
  }

}
