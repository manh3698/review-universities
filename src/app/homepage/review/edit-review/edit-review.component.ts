import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/map'
import { ReviewService } from '../review.service';
import { strict } from 'assert';

@Component({
  selector: 'app-edit-review',
  templateUrl: './edit-review.component.html',
  styleUrls: ['./edit-review.component.css']
})
export class EditReviewComponent implements OnInit {
  listUni ;

  myFiles: string [] = [];
  editReviewForm :  FormGroup;
  imagesData = []

  constructor(private reviewServces : ReviewService , public formBuilder : FormBuilder ,private router : Router, private route : ActivatedRoute) {
    this.editReviewForm = this.formBuilder.group({
      idUniver : String,
      content : [''],
      images : [],
    });
}

  ngOnInit(): void {
    this.reviewServces.getUni().subscribe((data: any)=>{
      this.listUni = data.data.data;
    });
  }

  uploadFile(event) {
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < event.target.files.length; i++) {
      this.myFiles.push(event.target.files[i]);
  }
}

  onEditReview(){
  //   const id = this.route.snapshot.paramMap.get('id');


  //   this.reviewServces.edit(this.editReviewForm.value, id).subscribe((data: any) => {
  //     this.router.navigateByUrl('review');
  //  });

     const id = this.route.snapshot.paramMap.get('id');
   const formData = new FormData();
   for (let i = 0; i < this.myFiles.length; i++) {
     formData.append('images', this.myFiles[i]);
   }

   this.reviewServces.uploadmutiple(formData).subscribe(
     (response: any) => {
       for(let i = 0 ; i < response.data.length ; i++){
         this.imagesData.push(response.data[i]._id)
       }
       this.editReviewForm.value.images = this.imagesData

       this.reviewServces.edit(this.editReviewForm.value , id).subscribe((data) => {
         alert("edit success");
         this.router.navigateByUrl('review');
       });
     },
     (error: HttpErrorResponse) => {
       alert(error.message);
     });

  }
}
