import { Component, OnInit } from '@angular/core';
import { ReviewService, ReviewModel } from '../review.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-review',
  templateUrl: './list-review.component.html',
  styleUrls: ['./list-review.component.css']
})
export class ListReviewComponent implements OnInit {
  listReview;
  page: number = 1;
  constructor(private reviewService : ReviewService , private router : Router) { }

  ngOnInit(): void {
    this.reviewService.listReview().subscribe((data:any)=>{
      this.listReview = data.data.data;
    })
  }
  deleteReview(review : ReviewModel){
    this.reviewService.delete(review._id).subscribe(
    (data : any) =>{
      this.router.navigateByUrl('review');
    })
    this.listReview = this.listReview.filter(u => u !== review);
  }

}
