import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  constructor(private httpClient: HttpClient) { }

  public listReview(){
    return this.httpClient.get( `http://3.12.85.162/review`);
  }
  public getUni(){
    return this.httpClient.get('http://3.12.85.162/uni');
  }

  public addreview(data : any){
    return this.httpClient.post(`http://3.12.85.162/review`, data);
  }
  public delete(id: string){
    return this.httpClient.delete(`http://3.12.85.162/review/${id}`);
  }
  public edit(data:any , id){
    return this.httpClient.put(`http://3.12.85.162/review/${id}`, data);
  }

  public upload(file) {
    return this.httpClient.post('http://3.12.85.162/image', file);
  }

  public uploadmutiple(files) {
    return this.httpClient.post('http://3.12.85.162/image/multi', files);
  }


  public getImage(){
    return this.httpClient.get('http://3.12.85.162/image');
  }

}

export class ReviewModel{
  _id : string;
  views : number;
  content : string;
  idUniver : string;
}

