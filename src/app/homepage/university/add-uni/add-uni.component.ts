import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { UniService } from '../uni.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Http, Response } from "@angular/http";
import 'rxjs/add/operator/map'

export type Item = { id: number, Title: string };



@Component({
  selector: 'app-add-uni',
  templateUrl: './add-uni.component.html',
  styleUrls: ['./add-uni.component.css']
})
export class AddUniComponent implements OnInit {
  items: Array<Item>;
  form: FormGroup;


  constructor(private uniService: UniService, public fb: FormBuilder, private router: Router,private http : Http) {
    this.form = this.fb.group({
      name: [''],
      address: [''],
      cate: [''],
      logo: ['']
    });
  }

  imageObj: File;
  imageUrl: string;
  listCate;


  ngOnInit(): void {
    this.uniService.getCate().subscribe((data: any)=>{
      this.listCate = data.data.data;
    });
    this.http
      .get('/assets/local.json')
      .map(data => data.json() as Array<Item>)
      .subscribe(data => {
        this.items = data;
      });
  }
  uploadFile(event) {
    const FILE = (event.target as HTMLInputElement).files[0];
    this.imageObj = FILE;
  }

  onSubmit() {
    const imageForm = new FormData();
    imageForm.append('image', this.imageObj);
    this.uniService.upload(imageForm).subscribe(
      (response: any) => {
        this.form.value.logo = response.data._id;
        this.uniService.addUni(this.form.value).subscribe((data) => {
          alert("add success");
          this.router.navigateByUrl('add-uni');
          location.reload();
        });
      },
      (error: HttpErrorResponse) => {
        console.log(error.error);
      });
  }

}
