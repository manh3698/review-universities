import { Component, OnInit } from '@angular/core';
import { UniService } from '../uni.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Http, Response } from "@angular/http";
import 'rxjs/add/operator/map'

export type Item = { id: number, Title: string };


@Component({
  selector: 'app-edit-uni',
  templateUrl: './edit-uni.component.html',
  styleUrls: ['./edit-uni.component.css']
})
export class EditUniComponent implements OnInit {
  form: FormGroup;
  items: Array<Item>;

  constructor(private uniService: UniService, public fb: FormBuilder, private route: ActivatedRoute, private router: Router,private http : Http) {
    this.form = this.fb.group({
      name: [''],
      address: [''],
      cate: [''],
      logo: ['']
    });
  }
  imageObj: File;
  imageUrl: string;
  editUni;
  listUni;
  listCate
  ngOnInit(): void {
    this.uniService.listUni().subscribe((data: any) => {
      this.listUni = data.data.data;
    });
    this.uniService.getCate().subscribe((data: any)=>{
      this.listCate = data.data.data;
    });
    this.http
      .get("/assets/local.json")
      .map(data => data.json() as Array<Item>)
      .subscribe(data => {
        this.items = data;
      });
  }
  uploadFile(event) {
    const FILE = (event.target as HTMLInputElement).files[0];
    this.imageObj = FILE;
  }

  onSubmit() {
    const imageForm = new FormData();
    imageForm.append('image', this.imageObj);
    const id = this.route.snapshot.paramMap.get('id');

    this.uniService.upload(imageForm).subscribe(
      (response: any) => {
        this.form.value.logo = response.data._id;
        this.uniService.editUni(this.form.value, id).subscribe((data: any) => {
          this.router.navigateByUrl('uni');
        });
      },
      (error: HttpErrorResponse) => {
        console.log(error.error);
      });
  }

}
