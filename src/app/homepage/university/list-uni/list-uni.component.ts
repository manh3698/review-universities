import { Component, OnInit } from '@angular/core';
import { UniService, UniModel } from '../uni.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-uni',
  templateUrl: './list-uni.component.html',
  styleUrls: ['./list-uni.component.css']
})
export class ListUniComponent implements OnInit {
  listUni;
  collection = [];
  p: number = 1;
  isImageLoading: boolean;
  imageService: any;
  imageToShow: string | ArrayBuffer;
  getImage;
  link: string | any;
  public img: string;

  page: number = 1;

  constructor(private uniService: UniService, private router : Router) {
   }

  ngOnInit(): void {
    this.uniService.listUni().subscribe((data: any) => {
      this.listUni = data.data.data;
    });
  }
  deleteUni(uni: UniModel){

    this.uniService.deleteUni(uni._id).subscribe(data => {
      this.router.navigateByUrl('uni');
      this.listUni = this.listUni.filter(u => u !== uni);
    });
  }
  loadImage() {
    this.uniService.getImage().subscribe(data => {
      //this.img = result;
    });
  }
}
