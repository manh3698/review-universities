import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class UniService {
  httpOptions = {
    headers: new HttpHeaders({
     'Content-Type': 'multipart/form-data' //
    })
  };
  data: any;

  constructor(private httpClient: HttpClient,private http:Http) { }



  public addUni(uni: UniModel){
    return this.httpClient.post(`http://3.12.85.162/uni`, uni);
  }

  public listUni(){
    return this.httpClient.get('http://3.12.85.162/uni?limit=50');

  }
  public deleteUni(id: string){
    return this.httpClient.delete('http://3.12.85.162/uni/' + id);
  }

  public editUni(data : any, id){
    return this.httpClient.put(`http://3.12.85.162/uni/${id}`, data);
  }

  public upload(file) {
    return this.httpClient.post('http://3.12.85.162/image', file);
  }


  public getCate(){
    return this.httpClient.get('http://3.12.85.162/cate');
  }
  public getImage(){
    return this.httpClient.get('http://3.12.85.162/image');
  }
}

export class UniModel{
  _id : string;
  name : string;
  address : string;
  logo : string|any;
  cate : string;
}
