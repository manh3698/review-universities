import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';

import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {


  addUserForm : FormGroup;
  constructor(private userService : UserService , private formBuilder : FormBuilder, private router: Router) {
            this.addUserForm = this.formBuilder.group({
              role : [''],
              email: [''],
              name : [''],
              avatar: [''],
              password : ['']
            });
        }
  imageObj: File;
  imageUrl: string;

  ngOnInit(): void {
  }

  uploadFile(event) {
    const FILE = (event.target as HTMLInputElement).files[0];
    this.imageObj = FILE;
  }

  onAddUser(){
    const imageForm = new FormData();
    imageForm.append('image', this.imageObj);
    this.userService.upload(imageForm).subscribe(
      (response: any) => {
        this.addUserForm.value.avatar = response.data._id;
        this.userService.addUser(this.addUserForm.value).subscribe((data) => {
          alert("add success");
          this.router.navigateByUrl('user');
          // location.reload();
        });
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      });
      }


}
