import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/map'



@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  editUserForm : FormGroup;
  constructor(private userService : UserService , public formBuilder : FormBuilder ,private router : Router, private route : ActivatedRoute) {
            this.editUserForm = this.formBuilder.group({
              role : [''],
              email: [''],
              name : [''],
              avatar : [''],
              password : ['']
            });
        }
  imageObj: File;
  imageUrl: string;

  ngOnInit(): void {}

  uploadFile(event) {
    const FILE = (event.target as HTMLInputElement).files[0];
    this.imageObj = FILE;
  }

  onEditUser(){
    const imageForm = new FormData();
    imageForm.append('image', this.imageObj);
    const id = this.route.snapshot.paramMap.get('id');

    this.userService.upload(imageForm).subscribe(
      (response: any) => {
        this.editUserForm.value.avatar = response.data._id;
        this.userService.editUser(this.editUserForm.value, id).subscribe((data: any) => {
          this.router.navigateByUrl('user');
        });
      },
      (error: HttpErrorResponse) => {
        console.log(error.error);
      });
      }

}
