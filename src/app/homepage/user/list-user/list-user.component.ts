import { Component, OnInit } from '@angular/core';
import { UserService, UserModel } from '../user.service';
import { Router } from '@angular/router';
import { filter } from 'rxjs/operators';


@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {

  listUser;
  page: number = 1;

  constructor(private UserService : UserService , private router : Router) {}

  ngOnInit(): void {
    this.UserService.listUer().subscribe((data:any)=>{
      this.listUser = data.data.data;

    })
  }
  deleteUser(user : UserModel){
      this.UserService.deleteUser(user._id).subscribe(
      (data : any) =>{
        this.router.navigateByUrl('user');
      })
      this.listUser = this.listUser.filter(u => u !== user);
    }

}

