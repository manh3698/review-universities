import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  httpOptions = {
    headers: new HttpHeaders({
     'Content-Type': 'multipart/form-data' //
    })
  };


  constructor(private httpClient : HttpClient) { }

  public addUser(user){
    return this.httpClient.post(`http://3.12.85.162/user/create`, user);
  }


  public listUer(){
    return this.httpClient.get('http://3.12.85.162/user/');

  }

  public editUser(data : any, id){
    return this.httpClient.put(`http://3.12.85.162/user/${id}`, data);
  }


  public deleteUser(id: string){
    return this.httpClient.delete(`http://3.12.85.162/user/${id}`);

  }

  public upload(file) {
    return this.httpClient.post('http://3.12.85.162/image', file);
  }
}

export class UserModel{
  _id : string;
  email : string;
  name : string;
  role : string;
}
