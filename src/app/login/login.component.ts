import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl('')
  });

  constructor(private apiService: ApiService, private router: Router) { }

  ngOnInit(): void {

  }

  onSubmit(){
    this.apiService.login(this.loginForm.value).subscribe((data: any) => {
      if (data && data.status === 200) {
        localStorage.setItem('token', data.data.token);
        this.router.navigateByUrl('home');
      }
      else if (data && data.status === 400) {
        // this.router.navigateByUrl('');
        location.reload();
      }
    });
  }

}
